data Tree a = Node a (Tree a) (Tree a)
            | Empty
              deriving (Show)

simpleTree = Node "parent" (Node "left child" Empty Empty)
                           (Node "right child" Empty Empty)

data TreeWithMaybe a = NodeWithMaybe a (Maybe (TreeWithMaybe a)) (Maybe (TreeWithMaybe a))
                       deriving (Show)

simpleTreeWithMaybe = NodeWithMaybe "parent" (Just (NodeWithMaybe "left child" Nothing Nothing))
                                             (Just (NodeWithMaybe "right child" Nothing Nothing))

nodesAreSame (Node a _ _) (Node b _ _)
    | a == b     = Just a
nodesAreSame _ _ = Nothing

