-- 1. and 2.

myLength :: [a] -> Int
myLength (x:xs) = 1 + (myLength xs)
myLength []     = 0

alphabet = ['a'..'z']
questionOneCheck = (length alphabet == myLength alphabet)


-- 3.

mySum (x:xs) = x + (mySum xs)
mySum []     = 0

myAverage xs = (mySum xs)/(fromIntegral (myLength xs))

